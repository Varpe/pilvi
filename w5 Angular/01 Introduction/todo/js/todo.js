
var app = angular.module('myApp', [])
app.controller('todoController', function($scope) {

  /* an array to store the list items */
  var items = Array()

  $scope.add = function($event) {
    console.log('add()')
    /* the $event object was passed from the view and contains useful information that can be used by the controller. */
    console.log('key code: '+$event.which)
    var keyCode = $event.which || $event.keyCode
    if (keyCode === 13) {
      console.log('enter key pressed')
      var newItem = $scope.newItem
      console.log(newItem)
      if (items.indexOf(newItem) == -1){
      items.push(newItem)
      console.log(items)
      $scope.newItem = ''
      $scope.items = items
      }
      else{
      	alert('duplicate entry')
      }
    }
  }
  $scope.delete = function(item) {
  	var index = $scope.items.indexOf(item);
  	$scope.items.splice(index,1);
    console.log('delete()')
  }
})


